INTRODUCTION
------------

The Custom Required Marker module allows you to set a custom marker for
required fields as well as the title attribute for the required field
marker's wrapper.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

The module adds one permission (Administer Custom Required Marker) and a
configuration page where a user with the added permission can set the
required field marker and the marker's wrapper's title attribute.

MAINTAINERS
-----------

Current maintainers:
 * Dave Bagler (Dave Bagler) - https://www.drupal.org/user/720712

This project has been sponsored by:
 * BAGLER IT INC.
