/**
 * @file
 * Add the custom required marker to dynamically added fields.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.custom_required_marker = {
    attach: function (context, settings) {
      $('.form-required').each(function () {
        var $this = $(this);
        $this.attr('title', settings.custom_required_marker.custom_required_marker_title);
        $this.text(settings.custom_required_marker.custom_required_marker_text);
      });

      $(document).bind('state:required', function (e) {
        if (e.trigger) {
          if (e.value) {
            var $label = $(e.target).closest('.form-item, .form-wrapper').find('label');
            // Avoids duplicate required markers on initialization.
            if (!$label.find('.form-required').length) {
              $label.append('<span class="form-required" title="'
                  + settings.custom_required_marker.custom_required_marker_title
                  + '">'
                  + settings.custom_required_marker.custom_required_marker_text
                  + '</span>');
            }
            else {
              $label.find('.form-required').replaceWith('<span class="form-required"'
                  + 'title="'
                  + settings.custom_required_marker.custom_required_marker_title
                  + '">'
                  + settings.custom_required_marker.custom_required_marker_text
                  + '</span>');
            }
          }
          else {
            $(e.target).closest('.form-item, .form-wrapper').find('label .form-required').remove();
          }
        }
      });
    }
  };
})(jQuery, Drupal);
